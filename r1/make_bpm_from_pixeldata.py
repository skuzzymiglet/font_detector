# make_bpm V1
# Given an image, generate a map of black and white pixels
# Remove antialiasing and backgrounds
# This will later become layeriso

from PIL import Image, ImageFilter, ImageDraw


def make_bpm(img):
    pixdata = img.load()
    img_size = (img.size[0], img.size[1])
    THRESHOLD = 160

    # BPM = Binary Pixel Map (B/W)
    bpm = [] 
    for x in range(img_size[0]):
        bpm.append([])
        for y in range(img_size[1]):
            bpm[x].append(not pixdata[x, y][0] > THRESHOLD)
            # bpm[x][y] = not (pixdata[x, y][0] > THRESHOLD)

    return bpm


img = Image.open("image.png")
img = img.convert("RGBA")
print(make_bpm(img))
