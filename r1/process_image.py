# CONTRIVED!! NOT A MODULE!!
# THIS IS LIKE layeriso WILL BE, BUT IT'S REALLY BASIC

from PIL import Image, ImageFilter
import os

img = Image.open("image.png")
img = img.convert("RGBA")
pixdata = img.load()

for y in range(img.size[1]):
    for x in range(img.size[0]):
        r, g, b, a = img.getpixel((x, y))
        if (r > 0) and (g > 0) and (b > 0):
            pixdata[x, y] = (0, 0, 0, 0)
            #Remove anti-aliasing outline of body.
            if r > 250 and g > 250 and b > 250:
                pixdata[x, y] = (255, 255, 255, 0)

img2 = img.filter(ImageFilter.GaussianBlur(radius=1))
img2.save("hi.png", "PNG")
