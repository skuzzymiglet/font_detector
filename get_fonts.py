import requests, json, urllib, os

API_KEY="AIzaSyChMEQqGQszG2sXKBBQj2PYIc6zjqz3aAU"
fonts_string = requests.get("https://www.googleapis.com/webfonts/v1/webfonts?key={}".format(API_KEY)).text
fonts_dict = json.loads(fonts_string)
families = fonts_dict["items"]
for i, font in enumerate(families):
    print(font["family"])
    files = font["files"]
    os.mkdir(font["family"])
    for _file in files:
        urllib.request.urlretrieve(files[_file], font["family"]+"/"+_file)
        
print(fonts_dict["items"][0]["family"])
